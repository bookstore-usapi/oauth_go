package oauth

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"strings"
	"time"

	httprest "github.com/mercadolibre/golang-restclient/rest"
	"gitlab.com/josafat.vargas.gamboa/oauth_go/oauth_go/utils/errors"
)

const (
	headerXPublic   = "X-Public"
	headerXClientId = "X-Client-Id"
	headerXCallerId = "X-User-Id"

	paramAccessToken = "access_token"
)

type accessToken struct {
	Id       string `jaon:"id"`
	UserID   int64  `jaon:"user_id"`
	ClientId int64  `jaon:"client_id"`
}

var (
	oauthRestClient = httprest.RequestBuilder{
		BaseURL: "http://localhost:9002",
		Timeout: 200 * time.Millisecond,
	}
)

func IsPublic(request *http.Request) bool {
	if request == nil {
		return true
	}

	return request.Header.Get(headerXPublic) == "true"
}

func GetCallerId(request *http.Request) int64 {
	if request == nil {
		return 0
	}

	id, err := strconv.ParseInt(request.Header.Get(headerXCallerId), 10, 64)
	if err != nil {
		return 0
	}
	return id
}

func GetClientId(request *http.Request) int64 {
	if request == nil {
		return 0
	}

	id, err := strconv.ParseInt(request.Header.Get(headerXCallerId), 10, 64)
	if err != nil {
		return 0
	}
	return id
}

func AuthenticateRequest(request *http.Request) *errors.RestErr {
	if request == nil {
		errors.NewBadRequestError("Invalid request for authentication")
	}

	cleanRequest(request)

	accessTokenId := strings.TrimSpace(request.URL.Query().Get(paramAccessToken))
	// https://api.bookstore.com/resource?access_token="asdf123"
	if accessTokenId == "" {
		return errors.NewBadRequestError("Empty access token ID")
	}

	at, err := getAccessToken(accessTokenId)
	if err != nil {
		if err.Status == http.StatusNotFound {
			return nil
		}
		return err
	}

	request.Header.Add(headerXClientId, fmt.Sprintf("%v", at.ClientId))
	request.Header.Add(headerXCallerId, fmt.Sprintf("%v", at.UserID))

	return nil
}

func cleanRequest(request *http.Request) {
	if request == nil {
		return
	}
	request.Header.Del(headerXPublic)
	request.Header.Del(headerXCallerId)
	//request.Header.Del(headerXClientId)
}

func getAccessToken(accessTokenId string) (*accessToken, *errors.RestErr) {
	response := oauthRestClient.Get(fmt.Sprintf("/oauth/access_token/%s", accessTokenId))
	// Timeout checks - received nil data
	if response == nil || response.Response == nil {
		return nil, errors.NewInternalServerError("Invalid client response when trying to get access token")
	}

	// Manage errors during the creation processs
	if response.StatusCode > 299 {
		var restErr errors.RestErr
		err := json.Unmarshal(response.Bytes(), &restErr)
		if err != nil {
			return nil, errors.NewInternalServerError("Login error: invalid error interface")
		}
		return nil, &restErr
	}

	// Convert resonse into user
	var at accessToken
	if err := json.Unmarshal(response.Bytes(), &at); err != nil {
		return nil, errors.NewInternalServerError("Error when trying to unmarshall response")
	}

	return &at, nil
}
